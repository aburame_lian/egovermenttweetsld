<!DOCTYPE html>
<html lang="en">
  <head>
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="js/jquery.min.js"></script>
	

    <title>Surabaya E-Gov Dashboard</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

		</style>
		
		<?php
// LOGIC
    set_include_path(get_include_path() . PATH_SEPARATOR . './lib/easyrdf/lib');
    require_once "EasyRdf.php";
    require_once "html_tag_helpers.php";
    // Setup some additional prefixes for DBpedia
    EasyRdf_Namespace::set('owl', 'http://www.w3.org/2002/07/owl#');
    EasyRdf_Namespace::set('rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#');
    EasyRdf_Namespace::set('gov', 'http://localhost/vayvis/ns/gov/');
    EasyRdf_Namespace::set('skos', 'https://www.w3.org/2009/08/skos-reference/skos.html#');
    $sparql = new EasyRdf_Sparql_Client('http://localhost:3030/PILancar/query');
    $query = ' PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix gov:  <http://localhost/vayvis/ns/gov/> 
prefix skos: <https://www.w3.org/2009/08/skos-reference/skos.html#>

select distinct ?label2 (SUM(?jml) as ?jml2)   where {
 ?l skos:broader ?s .
  ?s gov:jmlTweets ?jml .
    ?s rdfs:label ?label .
  ?parent skos:broader ?s .
  ?parent rdfs:label ?label2 ;
}
GROUP BY ?label2
ORDER BY ASC(?label2) 

';
    $result = $sparql->query(
        $query
    );
    $no = 1;


?>
		<script type="text/javascript">
$(function () {
    // Set up the chart
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'overall',
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 50,
                viewDistance: 25
            }
        }, 
        title: {
            text: 'Jumlah Tweets Berdasarkan Kategori'
        },
        subtitle: {
            text: 'Grafik ini menjelaskan Jumlah tweets berdasarkan kategorinya'
        },
		xAxis: {
                        categories: [
						<?php
        foreach ($result as $row) {
                                echo "'".str_replace('http://localhost/vayvis/ns/gov/','',$row->label2)."',";
                            }
        ?>
						
						]
                    },
        plotOptions: {
            column: {
                depth: 25
            },series: {
                colorByPoint: true
            }
        },
        series: [{name:'Total Tweets',
            data: [<?php
        foreach ($result as $row) {
            echo str_replace(',','.',$row->jml2).',';
        }
        ?>]
        }]
    });

    function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }

    // Activate the sliders
    $('#sliders input').on('input change', function () {
        chart.options.chart.options3d[this.id] = this.value;
        showValues();
        chart.redraw(false);
    });

    showValues();
});
		</script>
		


	<?php
// LOGIC
    set_include_path(get_include_path() . PATH_SEPARATOR . './lib/easyrdf/lib');
    require_once "EasyRdf.php";
    require_once "html_tag_helpers.php";
    // Setup some additional prefixes for DBpedia
    EasyRdf_Namespace::set('owl', 'http://www.w3.org/2002/07/owl#');
    EasyRdf_Namespace::set('rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#');
    EasyRdf_Namespace::set('gov', 'http://localhost/vayvis/ns/gov/');
    EasyRdf_Namespace::set('skos', 'https://www.w3.org/2009/08/skos-reference/skos.html#');
    $sparql = new EasyRdf_Sparql_Client('http://localhost:3030/PILancar/query');
    $query2 = 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix gov:  <http://localhost/vayvis/ns/gov/> 
prefix skos: <https://www.w3.org/2009/08/skos-reference/skos.html#>

select distinct ?label ?label2 (SUM(?jml) as ?jml2)   where {
 ?l skos:broader ?s .
  ?s gov:jmlTweets ?jml .
    ?s rdfs:label ?label .
  ?parent skos:broader ?s .
  ?parent rdfs:label ?label2 ;
}
GROUP BY ?label2 ?label
ORDER BY DESC(?label2) 


';
    $result2 = $sparql->query(
        $query2
    );
    $no = 1;
	
	$sparql = new EasyRdf_Sparql_Client('http://localhost:3030/PILancar/query');
    $query2 = 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix gov:  <http://localhost/vayvis/ns/gov/> 
prefix skos: <https://www.w3.org/2009/08/skos-reference/skos.html#>

select distinct ?label ?label2 (SUM(?jml) as ?jml2)   where {
 ?l skos:broader ?s .
  ?s gov:jmlTweets ?jml .
    ?s rdfs:label ?label .
  ?parent skos:broader ?s .
  ?parent rdfs:label ?label2 ;
}
GROUP BY ?label2 ?label
ORDER BY ASC(?label2) 


';
    $result2 = $sparql->query(
        $query2
    );
    $no = 1;
	


?>
		
		
		<script type="text/javascript">
$(function () {
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: "overall2",
            type: "column",
            borderWidth: 5,
            borderColor: '#e8eaeb',
            borderRadius: 0,
            backgroundColor: '#f7f7f7'
        },
        title: {
            style: {
                'fontSize': '1em'
            },
            useHTML: true,
            x: -27,
            y: 8,
            text: 'Jumlah Tweet berdasarkan category dan subcategorynya'
        },
        series: [{
            data: [<?php
        foreach ($result2 as $row) {
            echo str_replace(',','.',$row->jml2).',';
        }
        ?>]
        }],
        xAxis: {
			
            categories: [ <?php foreach ($result as $row) {
                                echo "{name:'".str_replace('Kategori','',$row->label2)."',categories:[";
								foreach ($result2 as $row2){
									$r1=$row->label2;
									$r2=$row2->label2;
									if((string)$r2==(string)$r1){
										echo "'".str_replace('Sub Kategori','',$row2->label)."',";
									}
										
								}
								
								echo "]},";
                            }?>
							]
        }
    });
});
		</script>
	
  </head>
  <body>
  <script src="js/highcharts.js"></script>
<script src="js/highcharts-3d.js"></script>
<script src="js/modules/exporting.js"></script>
<script src="js/grouped-categories.js"></script>
	 <script type="text/javascript">
    document.getElementById("dash").setAttribute("class","active");
    </script>    
    <div class="container">
        <div class="page-header">
            <h1>Surabaya E-Gov Dashboard</h1>

            <ul class="nav nav-pills" role="tablist">
                <li role="presentation" class="active"><a href="#cost" aria-controls="cost" role="tab" data-toggle="tab">Categories</a></li>
                <li role="presentation"><a href="#total" aria-controls="total" role="tab" data-toggle="tab">Sub Categories </a></li>
         
               
            </ul>
        </div>
    <div> </div>
    <div><h3>Report Overview</h3></div>
    </div>
	<div class="container">
	
		<div class="row">
			<div class="col-md-8">
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="cost">
						
						<div class="jumbotron" id="overall">
						
						</div>
						
				<div id="sliders">
							<table>
								<tr>
									<td>Alpha Angle</td>
									<td><input id="alpha" type="range" min="0" max="45" value="15"/> <span id="alpha-value" class="value"></span></td>
								</tr>
								<tr>
									<td>Beta Angle</td>
									<td><input id="beta" type="range" min="-45" max="45" value="15"/> <span id="beta-value" class="value"></span></td>
								</tr>
								<tr>
									<td>Depth</td>
									<td><input id="depth" type="range" min="20" max="100" value="50"/> <span id="depth-value" class="value"></span></td>
								</tr>
							</table>
						</div>
			
						<br></br>
						
						<h3>Data Kategorikal</h3>
						<h4>Berikut merupakan laporan tweets dari akun e-goverment surabaya berdasarkan kategori</h4>
						<br></br>
						
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>Kategori</th>
            <th>Jumlah</th>
           
        </tr>
    </thead>
    <tbody>
  <?php foreach ($result as $row) { 
    echo "<tr>";
        echo "<td>" . str_replace('http://localhost/vayvis/ns/gov/','',$row->label2) . "</td>";
        echo "<td>" . str_replace('http://localhost/vayvis/ns/gov/','',$row->jml2) . "</td>";
	
		echo "</tr>" ;
  }?>
</tbody>
</table>

<br></br>
						
					</div>

					<div role="tabpanel" class="tab-pane fade" id="total">
						
						<div class="container" id="overall2">					
						</div>
						<br></br>
						<h3>Data Subkategorikal</h3>
						<h4>Berikut data mengenai pemberitaan Tweets yang dilakukan oleh akun e-goverment di Surabaya berdasarkan Sub Kategori </h4>
						<br></br>
						
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>Kategori</th>
            <th>Sub-Kategori</th>
            <th>Jumlah Tweets</th>
        </tr>
    </thead>
    <tbody>
  <?php foreach ($result2 as $row) { 
    echo "<tr>";
        echo "<td>" . str_replace('http://localhost/vayvis/ns/gov/','',$row->label2) . "</td>";
		echo "<td>" . str_replace('http://localhost/vayvis/ns/gov/','',$row->label) . "</td>";
        echo "<td>" . str_replace('http://localhost/vayvis/ns/gov/','',$row->jml2) . "</td>";
	
		echo "</tr>" ;
  }?>
</tbody>
</table>
						
									

					</div>
					
				</div>
			</div>
			
		</div>
	</div>

	
	<div id="container"></div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="api/js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	
  </body>
</html>